Group: group 45

Question
========

RQ: Is there a correlation between the GDP of the United Kingdom and its suicide rate?

Null hypothesis: There is no correlation between the GDP of the United Kingdom and its suicide rate.

Alternative hypothesis: There is a correlation between the GDP of the United Kingdom and its suicide rate.

Dataset
=======

URL: https://www.kaggle.com/russellyates88/suicide-rates-overview-1985-to-2016

Column Headings:

> suicides <- read.csv("dataset.csv")
> colnames(suicides)
 [1] "country"            "year"               "sex"                "age"                "suicides_no"        "population"         "suicides.100k.pop"  "country.year"
 [9] "HDI.for.year"       "gdp_for_year...."   "gdp_per_capita...." "generation"
